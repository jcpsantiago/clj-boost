# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## 1.0.0 - 2019-6-18
### API freeze
- Frozen API
- Blocked further development
- Bump *XGBoost* version to 0.90

## 0.0.4 - 2019-1-11
### Locale bug fix
- Fixed a locale bug: `cross-validation` broke down with `,` as decimal separator
- Now `dmatrix` has also an identity version: if given a **DMatrix** returns the same

## 0.0.3 - 2018-12-1
### Beauty release
- Remove `tools.deps` and `codeina` from `:dev` dependencies
- Refactoring of *demo/tutorial.clj* code
- Change docstrings to improve cljdoc visualization
- Create dev branch to leave master cleaner for tools.deps
- Bump *XGBoost* to version 0.81
- Wrote tutorial both in *doc/getting-started.md* and on blog (see README)

## 0.0.2 - 2018-10-12
### Re-release
- Bumped XGBoost version to 0.80
- Improved *project.clj* moving unneeded dependencies to `:profiles`
- Refactoring of `cross-validation`, usage and return are the same

## 0.0.1 - 2018-09-23
### First release
- Released on Clojars
